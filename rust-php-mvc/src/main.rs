use std::fs;
use std::fs::{File, OpenOptions};
use std::io;
use std::io::prelude::*;
use std::os::unix;
use std::path::Path;
// I can't believe I just built this ! ??  Freaking Epic ! ~kg //
fn cat(path: &Path) -> io::Result<String> {
    let mut f = try!(File::open(path));
    let mut s = String::new();
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

// A simple implementation of `% echo s > path`
fn echo(s: &str, path: &Path) -> io::Result<()> {
    let mut f = try!(File::create(path));

    f.write_all(s.as_bytes())
}

// A simple implementation of `% touch path` (ignores existing files)
fn touch(path: &Path) -> io::Result<()> {
    match OpenOptions::new().create(true).open(path) {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}
fn main() {
    println!("`mkdir ../app`");
    println!("`mkdir ../public_html`");
    println!("`mkdir ../public_html/js`");
    println!("`mkdir ../public_html/css`");
    println!("`mkdir ../app/core`");
    println!("`mkdir ../app/controllers`");
    println!("`mkdir ../app/models`");
    println!("`mkdir ../app/views`");
    println!("`mkdir ../app/views/home`");

    match fs::create_dir("../app") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../public_html") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../public_html/js") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../public_html/css") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../app/core") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../app/controllers") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../app/models") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../app/views") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
    match fs::create_dir("../app/views/home") {
        Err(why) => println!("! {:?}", why.kind()),
        Ok(_) => {},
    }
 println!("`touch ../public_html/README.md`");
    touch(&Path::new("../public_html/README.md")).unwrap_or_else(|why| {
    println!("! {:?}", why.kind());
    });
 println!("`ln -s ../app/README.md ../public_html/README.md`");
    if cfg!(target_family = "unix") {
        unix::fs::symlink("../app/README.md", "../public_html/README.md").unwrap_or_else(|why| {
        println!("! {:?}", why.kind());
        });
    }
  println!("`cat ../public_html/README.md`");
        match cat(&Path::new("../app/README.md")) {
            Err(why) => println!("! {:?}", why.kind()),
            Ok(s) => println!("> {}", s),
    }
  println!("`echo Kristian Gali's Custom Base Framework '\n' This custom php mvc was written by Kristian Gali and compiled by rust-lang.org. '\n' For more information go to http://www.kristiangali.com/code-tutorials/ '\n' where I give a free demonstration of how I did this. '\n' Thank you for viewing my code. '\n' All Rights are Reserved. Please do not copy my code or else all legal copyright penalties will basely. '\n' thank you! ~kg > q/README.md`");
  echo("Kristian Gali's Custom Base Framework '\n' This custom php mvc was written by Kristian Gali and compiled by rust-lang.org. '\n' For more information go to http://www.kristiangali.com/code-tutorials/ '\n' where I give a free demonstration of how I did this. '\n' Thank you for viewing my code. '\n' All Rights are Reserved. Please do not copy my code or else all legal copyright penalties will basely. '\n' thank you! ~kg ", &Path::new("../app/README.md")).unwrap_or_else(|why| {
  println!("! {:?}", why.kind());
      });
      let mut init = File::create("../app/init.php").unwrap();
      init.write_all(b"<?php error_reporting(E_ALL); ini_set('display_errors', 1);
      /**
      * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
      * @author Kristian A. Gali <kg@kristiangali.com>
      * @version 1.0.1
      * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
      */
      require_once __DIR__.'/core/App.php';
      require_once __DIR__.'/core/Controller.php';
      ?> ").expect("Couldn't create file");
    let mut htpub = File::create("../public_html/.htaccess").unwrap();
    htpub.write_all(b"
    Options -MultiViews
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]").expect("Couldn't create file");
    let mut htpriv = File::create("../app/.htaccess").unwrap();
        htpriv.write_all(b"Options -indexes").expect("Couldn't create file");
    let mut base = File::create("../app/core/App.php").unwrap();
        base.write_all(b"<?php error_reporting(E_ALL); ini_set('display_errors', 1);
        /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        class App
        {
            protected $controller   = 'home';
            protected $method       = 'index';
            protected $params       = [];
            public function __construct()
            {
                print_r($this->parseUrl());
                $url = $this->parseUrl();
                if(file_exists('../app/controllers/' . $url[0] . '.php'))
                {
                    $this->controller = $url[0];
                    unset($url[0]);
                }
                require_once '../app/controllers/' . $this->controller . '.php';
                $this->controller = new $this->controller;
                if(isset($url[1]))
                {
                    if(method_exists($this->controller, $url[1]))
                    {
                        $this->method = $url[1];
                        unset($url[1]);
                    }
                }
                $this->params = $url ? array_values($url) : [];
                call_user_func_array([$this->controller, $this->method], $this->params);
            }
            public function parseUrl()
            {
                if(isset($_GET['url']))
                {
                    return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
                }
            }
        } ").expect("Couldn't create file");
        let mut model = File::create("../app/core/Controller.php").unwrap();
        model.write_all(b"<?php
        /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        class Controller
        {
            protected $base;
            public function __construct()
            {
                $base = '../app/';
                $this->base = $base;
            }
            public function model($model)
                {
                    require_once $this->base . 'models/' . $model . '.php';
                    return new $model();
                }
            public function view($view, $data = [])
                {
                    require_once $this->base . 'views/' . $view . '.php';
                }
        }?> ").expect("Couldn't create file");
        let mut css = File::create("../public_html/css/style.css").unwrap();
        let mut js = File::create("../public_html/js/custom.js").unwrap();
        css.write_all(b"// Custom CSS //").expect("Couldn't create file");
        js.write_all(b"//custom JS //").expect("Couldn't create file");
        let mut view = File::create("../public_html/index.php").unwrap();
        view.write_all(b"<?php /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        require_once '../app/init.php';
        $app = new App;
        ?> ").expect("Couldn't create file");
        let mut users = File::create("../app/models/User.php").unwrap();
        users.write_all(b"<?php /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        class User
        {
            public $name = 'Guest';
        } ?> ").expect("Couldn't create file");
        let mut home = File::create("../app/controllers/home.php").unwrap();
        home.write_all(b"<?php /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        class Home extends Controller
        {
            protected $role;
            protected $user;

            public function index($name = '', $role = '')
            {
        $user = $this->model('User');
        $role = 'Visitor';
        $name = 'Guest';
        $user->name = $name . ' ' . $role;
        $this->view('home/index', ['name' => $user->name]);
            }
        }
        ?> ").expect("Couldn't create file");
        let mut home_index = File::create("../app/views/home/index.php").unwrap();
        home_index.write_all(b"<?php error_reporting(E_ALL); ini_set('display_errors', 1);
        /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        echo $data['name'];
        ?> ").expect("Couldn't create file");
        let mut roles = File::create("../app/models/Role.php").unwrap();
        roles.write_all(b"<?php error_reporting(E_ALL); ini_set('display_errors', 1);
        /**
        * Auto generated by Kristian Gali custom ~base MVC Generator and rust-lang
        * @author Kristian A. Gali <kg@kristiangali.com>
        * @version 1.0.1
        * @license (c) All Rights Reserved ~kgbase www.kristiangali.com/license
        */
        class Role
        {
            public $role = 'Visitor';
        } ?> ").expect("Couldn't create file");
        println!("`ls app`");
        match fs::read_dir("app") {
            Err(why) => println!("! {:?}", why.kind()),
            Ok(paths) => for path in paths {
                println!("> {:?}", path.unwrap().path());
            },
        }
    }
