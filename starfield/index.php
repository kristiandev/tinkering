<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>StarField_p5.js</title>
    <script src="p5/p5.js" type="text/javascript"></script>

    <script src="p5/addons/p5.dom.js" type="text/javascript"></script>
    <script src="p5/addons/p5.sound.js" type="text/javascript"></script>

    <script src="theory.js" type="text/javascript"></script>
    <script src="star.js" type="text/javascript"></script>
    <style> body {padding: 0; margin: 0;} canvas {vertical-align: top;} </style>
  </head>
  <body>
  <div style="position:absolute;color:white;z-index:9;margin-top:150px;margin-left:430px;font-size:36px;">
  	<img style="opacity:0.5;" src="SpeedOfLight.png" alt="">
  	</div>
  </body>
</html>