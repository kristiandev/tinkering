var stars = [];
var wormholes = [];
var speed;

function setup() {
  createCanvas(1170, 500);
  for (var i = 0; i < 500; i++) {
    stars[i] = new Star();
  }
  for(var wh = 0; wh < 15; wh++) {
    wormholes[i] = new wormHole();
  }
}

function draw() {
  speed = map(mouseX, 0, width, 0, 50);
  background(0);
  translate(width / 2, height / 2);
  for (var i = 0; i < stars.length; i++) {
    stars[i].update();
    stars[i].show();
  }
  for (var wh = 0; wh < wormholes.length; wh++) {
    	wormholes[i].update();
    	wormholes[i].show();
	}
}