# README

# Setting up the environment for Rails is a little tougher than I'd anticipated, granted I was being a dad more than studying.  
# 1) When I get my environment set up I first go to: 
# rubyinstaller.org/downloads and download 2 things.  Ruby 2.3.3 (and install) and the DevKit
# When I download the DevKit I need to cd into it and use gem to install the debugger as well as rails
# I may have errors after, but the commands will be there to help me install everything necessary to get rails up
# 2) I do gem install on all required gems and rails
# 3) I then bundle everything after there are no errors. 
# 4) After I bundle everything, I can then run `rails server` and check the listening host & port 
# 5) THE MVC folders are located in the app directory

* ...
