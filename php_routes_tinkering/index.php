<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);
/**
* @author Kristian Gali <kristian@kristiangali.com>
* Custom Route Class
*/
require 'core/route.class.php';
$frame_base = 'http://' .$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$index = new Route();
$index->pull($frame_base);
$index->getRoute($frame_base, $routes);

?><!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Home Page</title>
		<!-- Latest compiled and minified CSS & JS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</head>
	<body>
	<div class="container">
	<hr>
	<?php
	echo "<pre>";
	print_r($index);
	echo "</pre>";
	if(isset($index->route['home']))
	{
		switch($index->route['home'])
		{
			case "guest":
			echo "Hi Guest";
			break;
			case "admin":
			echo "Hi Admin";
			break;
			case "user":
			echo "Hi User";
			break;
			default:
			$index->route['home'] = "";
			echo "visitor";
			break;
			
		}
	}

?>	</div>
	</body>
	</html>
