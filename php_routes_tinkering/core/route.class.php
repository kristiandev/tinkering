<?php 

/**
* @author Kristian Gali <kristian@kristiangali.com>
* Custom Route Class
*/

class Route
{
	public $url;
	public $query_input;
	public $route = array();

	public function pull($url)
    {
    	return $this->url = parse_url($url);
    }
    public function getRoute($url, &$route)
    {
    	$this->route = $route;
    	$url_parts = parse_url($url);
    	if(!empty($url_parts['query'])) 
    	{
		return $this->query_input = parse_str($url_parts['query'], $this->route);
		}
		else 
		{
			echo "No query made.";
		}
    }

}
