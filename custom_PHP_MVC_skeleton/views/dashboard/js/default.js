$(function() {
    
    $.get('dashboard/xhrGetStatuses', function(o) {  
        for (var i = 0; i < o.length; i++)
        {
            $('#listStatuses').append('<div class="myStatus">\"' + o[i].status + '\" <p style="font-size:10px;">posted on: ' + o[i].created + '</p><a class="del" rel="' + o[i].stid + '" href="#delStatus">&nbsp;x</a></div>');
        }
        $('div').on('click', "a.del", function(){
            var stid = $(this).attr('rel');
            $.post('dashboard/xhrDeleteStatus', {'stid': stid}, function(o) {
                $(this).parent().remove();
            }, 'json');
            window.location.href += "#delStatus";
            location.reload();
        });     
    }, 'json');

    $('#sayStatus').submit(function() {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.post(url, data, function(o) {
            $('#listStatuses').append('<div class="myStatus">\"' + o.status + '\" <p style="font-size:10px;">posted on: '+ o.created + '</p><a class="del" rel="'+ o.stid +'" href="#delStatus">&nbsp;x</a></div>');        
        }, 'json');
        window.location.href += "#delStatus";
        location.reload();
        return false;
    });

});