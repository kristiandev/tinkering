<div class="dashboard-page">
	<div class="container">
		<div class="col-md-6">
		What's new with you today?
				<br />
					<form role="form" id="sayStatus" action="<?php echo URL;?>dashboard/xhrStatus/" method="POST">
						<div class="form-group">
						    <textarea class="form-control" rows='4' type="text" name="status"></textarea>
						    <input type="hidden" name="uid_created" value="<?php echo $_SESSION['uid_created']; ?>">
						    <input type="hidden" name="created" value="<?php echo date('Y-m-d H:i:s'); ?>">
						</div>
					    	<input class="btn btn-success btn-block" type="submit" />
					</form>
				<br />
		</div>
		<div class="col-md-6 statusbg">
		<h4>My Recent Statuses</h4>
				<div id="listStatuses">
				</div>
		</div>
	</div>
</div>