<div class="login-page">
	<div style="height:500px;" class="container">
	<br><br>
		<div style="padding:20px;" class="col-md-6 col-md-offset-3">
			<div class="login-form">
				<form action="login/run" method="POST" role="form">
					<legend style="color:#fff;text-align: center;">The Skeleton Framework</legend>
					<div class="form-group">
						<label for="">Email</label>
						<input type="email" name="email" class="form-control" id="email" placeholder="Email" required="required">
					</div>
					<div class="form-group">
						<label for="">Password</label>
						<input type="password" name="password" class="form-control" id="password" placeholder="********" required="required">
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>