<div class="users-page">
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
        <?php
        // print_r($this->user);
        ?>  <form role="form" method="post" action="<?php echo URL;?>user/editSave/<?php echo $this->user[0]['uid']; ?>">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" value="<?php echo $this->user[0]['email']; ?>" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="password" class="form-control" required="required">
                </div>
                <label>Role</label>
                <div class="form-group">
                    <select name="role" class="form-control">
                        <option value="visitor" <?php if($this->user[0]['role'] == 'visitor') echo 'selected'; ?>>Visitor</option>
                        <option value="guest" <?php if($this->user[0]['role'] == 'guest') echo 'selected'; ?>>Guest</option>
                        <option value="member" <?php if($this->user[0]['role'] == 'owner') echo 'selected'; ?>>Member</option>
                        <option value="admin" <?php if($this->user[0]['role'] == 'admin') echo 'selected'; ?>>Admin</option>
                        <option value="owner" <?php if($this->user[0]['role'] == 'owner') echo 'selected'; ?>>Owner</option>
                    </select>
                </div>
                <input class="btn btn-success btn-block" type="submit" />
            </form>
        </div>
    </div>
</div>