<div class="users-page">
    <div class="container">
        <div class="col-md-6">
            <form role="form" method="post" action="<?php echo URL;?>user/create">
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="text" name="email" required="required">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="text" name="password" required="required">
                </div>
                <div class="form-group">
                    <label>Role</label>
                        <select class="form-control" name="role">
                            <option value="visitor">Visitor</option>
                            <option value="guest">Guest</option>
                            <option value="member">Member</option>
                            <option value="admin">Admin</option>
                        </select>
                </div>
                <input class="btn btn-success btn-block" type="submit" />
            </form>
        </div>
        <div class="col-md-6">
            <table class="table table-striped">
                <?php
                    foreach($this->userList as $key => $value) {
                        echo '<tr>';
                        echo '<td>' . $value['uid'] . '</td>';
                        echo '<td>' . $value['email'] . '</td>';
                        echo '<td>' . $value['role'] . '</td>';
                        echo '<td>
                                <a href="'.URL.'user/edit/'.$value['uid'].'">Edit</a> 
                                <a href="'.URL.'user/delete/'.$value['uid'].'">Delete</a></td>';
                        echo '</tr>';
                    }
                ?>
            </table>
        </div>
    </div>
</div>