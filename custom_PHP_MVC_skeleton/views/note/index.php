<div class="notes-page">
<div class="container">
<div class="col-md-6">
<form role="form" method="post" action="<?php echo URL;?>note/create">
<legend style="text-align:center">SAVE IMPORTANT NOTES HERE</legend>
<div class="form-group">
    <input class="form-control" type="text" name="title" placeholder="Note Title" required="required">
    <br>
    <textarea class="form-control" placeholder="Body" name="content" required="required"></textarea>
</div>
    <label>&nbsp;</label><input class="btn btn-success btn-block" type="submit" />
</form>
</div>
<div class="col-md-6">
<table class="table table-striped">
<?php
    foreach($this->noteList as $key => $value) {
        echo '<tr>';
        echo '<td>' . $value['title'] . '</td>';
        echo '<td>' . $value['created'] . '</td>';
        echo '<td><a href="'. URL . 'note/edit/' . $value['nid'].'">Edit</a></td>';
        echo '<td><a class="delete" href="'. URL . 'note/delete/' . $value['nid'].'">Delete</a></td>';
        echo '</tr>';
    }
?>
</table>
</div>
</div>
</div>
<script>
$(function() {
    
    $('.delete').click(function(e) {
        var c = confirm("Are you sure you want to delete this note? ");
        if (c == false) return false;
        
    });
    
});
</script>