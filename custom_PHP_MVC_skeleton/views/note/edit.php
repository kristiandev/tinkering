<div class="notes-page">
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<form role="form" method="post" action="<?php echo URL;?>note/editSave/<?php echo $this->note[0]['nid']; ?>">
				<legend style="text-align:center">EDIT & UPDATE</legend>
					<div class="form-group">
					<label for="title">Title</label>
					    <input type="text" class="form-control" name="title" value="<?php echo $this->note[0]['title']; ?>" />
					    <br>
					<label for="content">Body</label>
						<textarea class="form-control"  name="content"><?php echo $this->note[0]['content'];?></textarea><br />
					</div>
			    <input class="btn btn-success btn-block" type="submit" value="Update Note" />
			</form>
		</div>
	</div>
</div>