<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=(isset($this->title)) ? $this->title : 'The Ultimate PHP MVC Skeleton'; ?>">
    <meta name="author" content="Kristian Aaron Gali - <kristiangali@gmail.com>">
    <title><?=(isset($this->title)) ? $this->title : 'MVC Skeleton'; ?></title> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">    
        <!-- Bootstrap Core CSS -->
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.4/angular.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/bootstrap.min.js"></script>
    <?php 
    if (isset($this->js)) 
    {
        foreach ($this->js as $js)
        {
            echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
        }
    }
    ?>
</head>
<body>
<?php Session::init(); ?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo URL; ?>index">MVC Skeleton</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav"> 
        <?php if (Session::get('loggedIn') == true):?>
        <li><a href="<?php echo URL; ?>dashboard">Dashboard</a></li>
        <li><a href="<?php echo URL; ?>note">Notes</a></li> 
        <?php if (Session::get('role') == 'owner'):?>
        <li><a href="<?php echo URL; ?>user">Users</a></li>
        <?php endif; ?> 
        <li><a href="<?php echo URL; ?>dashboard/logout">Logout</a></li>    
        <?php else: ?>
        <?php endif; ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if (Session::get('loggedIn') == false):?>
      <li><a href="<?php echo URL; ?>login">Login</a></li>
      <?php endif; ?> 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Documentation <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo URL; ?>help">The Wiki</a></li>
            <?php if (Session::get('loggedIn') == true):?>
            <li><a href="#">Developer Practices</a></li>
            <?php endif; ?> 
            <li><a href="#">Developer Api</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container-fluid" id="content">