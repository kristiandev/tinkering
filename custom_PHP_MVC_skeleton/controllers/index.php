<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        //echo Hash::create('sha256', 'YourPasswordHere', HASH_PASSWORD_KEY);
        $this->view->title = 'Home';
        $this->view->render('index/index');
    }
    
}