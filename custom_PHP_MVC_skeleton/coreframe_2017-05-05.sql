# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: coreframe
# Generation Time: 2017-05-05 19:48:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table friends
# ------------------------------------------------------------

DROP TABLE IF EXISTS `friends`;

CREATE TABLE `friends` (
  `frid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid_created` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `object` longtext NOT NULL COMMENT 'serialized friend id object',
  PRIMARY KEY (`frid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `nid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid_created` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;

INSERT INTO `notes` (`nid`, `uid_created`, `title`, `content`, `created`, `modified`)
VALUES
	(6,2,'testnote','test','2017-05-03 21:57:21','0000-00-00 00:00:00'),
	(8,1,'My New Note today','This is a new note to remind me to do something.','2017-05-04 07:52:23','0000-00-00 00:00:00'),
	(9,1,'Here is another','I have another in mind too','2017-05-04 07:52:35','0000-00-00 00:00:00'),
	(10,1,'Don\'t forget about this one','Yup I came through too','2017-05-04 07:52:50','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `stid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid_created` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `is_current` int(4) NOT NULL DEFAULT '0',
  `status` longtext NOT NULL,
  PRIMARY KEY (`stid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;

INSERT INTO `statuses` (`stid`, `uid_created`, `created`, `modified`, `is_current`, `status`)
VALUES
	(166,1,'2017-05-04 07:53:39','0000-00-00 00:00:00',0,'Here is my status'),
	(169,1,'2017-05-04 23:56:14','0000-00-00 00:00:00',0,'I love life\r\n');

/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `tid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid_created` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(150) NOT NULL DEFAULT '',
  `value` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;

INSERT INTO `tokens` (`tid`, `uid_created`, `created`, `modified`, `name`, `value`)
VALUES
	(1,0,'2017-04-26 09:19:00','2017-04-26 09:19:00','userid','<uid></uid>'),
	(2,0,'2017-04-26 09:20:00','2017-04-26 09:20:00','username','<username></username>'),
	(3,0,'2017-04-26 09:21:00','2017-04-26 09:21:00','firstname','<firstname></firstname>'),
	(4,0,'2017-04-26 09:22:00','2017-04-26 09:22:00','lastname','<lastname></lastname>'),
	(5,0,'2017-04-26 09:22:00','2017-04-26 09:22:00','email','<email></email>'),
	(6,0,'2017-04-26 09:23:00','2017-04-26 09:23:00','password','<password></password>'),
	(7,0,'2017-04-26 09:24:00','2017-04-26 09:24:00','birthdate','<birthdate></birthdate>'),
	(8,0,'2017-04-26 09:25:00','2017-04-26 09:25:00','address_one','<addressone></addressone>'),
	(9,0,'2017-04-26 09:26:00','2017-04-26 09:26:00','address_two','<addresstwo></addresstwo>'),
	(10,0,'2017-04-26 09:27:00','2017-04-26 09:27:00','city_one','<cityone></cityone>'),
	(11,0,'2017-04-26 09:27:00','2017-04-26 09:27:00','city_two','<citytwo></citytwo>'),
	(12,0,'2017-04-26 09:28:00','2017-04-26 09:28:00','state_one','<stateone></stateone>'),
	(13,0,'2017-04-26 09:28:00','2017-04-26 09:28:00','state_two','<statetwo></statetwo>'),
	(14,0,'2017-04-26 09:28:00','2017-04-26 09:28:00','country','<country></country>'),
	(15,0,'2017-04-26 09:30:00','2017-04-26 09:30:00','facebook','<facebook></facebook>'),
	(16,0,'2017-04-26 09:31:00','2017-04-26 09:31:00','linkedin','<linkedin></linkedin>'),
	(17,0,'2017-04-26 09:32:00','2017-04-26 09:32:00','googleplus','<googleplus></googleplus>'),
	(18,0,'2017-04-26 09:33:00','2017-04-26 09:33:00','status','<status></status>'),
	(19,0,'2017-04-26 09:35:00','2017-04-26 09:35:00','note','<note></note>'),
	(20,0,'2017-04-26 09:36:00','2017-04-26 09:36:00','fullname','<firstname></firstname><middlename></middlename><lastname></lastname>'),
	(21,0,'2017-04-26 09:37:00','2017-04-26 09:37:00','middlename','<middlename></middlename>'),
	(22,0,'2017-04-26 09:41:00','2017-04-26 09:41:00','zipcode','<zipcode></zipcode>'),
	(23,0,'2017-04-26 09:41:00','2017-04-26 09:41:00','nid','<noteid></noteid>'),
	(24,0,'2017-04-26 09:41:00','2017-04-26 09:41:00','stid','<statusid></statusid>'),
	(25,0,'2017-04-26 10:11:00','2017-04-26 10:11:00','frid','<friendid></friendid>');

/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(120) NOT NULL DEFAULT '',
  `avatar` varchar(200) DEFAULT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `birthdate` datetime DEFAULT NULL,
  `address_one` varchar(255) DEFAULT '',
  `address_two` varchar(255) DEFAULT '',
  `city_one` varchar(120) DEFAULT '',
  `city_two` varchar(120) DEFAULT '',
  `state_one` varchar(120) DEFAULT '',
  `state_two` varchar(120) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `zip_code` varchar(6) DEFAULT '',
  `facebook` varchar(100) DEFAULT '',
  `linkedin` varchar(100) DEFAULT '',
  `googleplus` varchar(100) DEFAULT '',
  `status` longtext,
  `role` enum('visitor','guest','member','admin','owner') NOT NULL DEFAULT 'visitor',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`uid`, `username`, `created`, `modified`, `firstname`, `lastname`, `email`, `avatar`, `password`, `birthdate`, `address_one`, `address_two`, `city_one`, `city_two`, `state_one`, `state_two`, `country`, `zip_code`, `facebook`, `linkedin`, `googleplus`, `status`, `role`)
VALUES
	(1,'admin','0000-00-00 00:00:00','2017-05-04 23:56:09','Kristian','Gali','kristiangali@gmail.com','kg.png','e436d1534f8aaa40226f825a0692281b4404745fd77d9cef0c9385d6128234df','0000-00-00 00:00:00','9300 South Redwood Rd #1512','','West Jordan','','Utah','','US','84088','http://facebook.com','http://linkedin.com','http://plus.googl.com','Please Update Your Status. Deleted on: 2017-05-04 23:56:09','owner'),
	(4,'','0000-00-00 00:00:00',NULL,'','','test@testingemail.com',NULL,'bc02a37484a3cd0ca915f77a2dabfa5cba1f2b62942cd85a9b2c7df68fa6fa41',NULL,'','','','','','','','','','','',NULL,'member');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
