<?php //error_reporting(E_ALL); ini_set('display_errors', 1); 

class User_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function userList()
    {
        return $this->db->select('SELECT uid, email, role FROM users');
    }
    
    public function userSingleList($uid)
    {
        return $this->db->select('SELECT uid, email, role FROM users WHERE uid = :uid', array(':uid' => $uid));
    }
    
    public function create($data)
    {
        $this->db->insert('users', array(
            'email' => $data['email'],
            'password' => Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'role' => $data['role']
        ));
    }
    
    public function editSave($data)
    {
        $postData = array(
            'email' => $data['email'],
            'password' => Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'role' => $data['role']
        );
        
        $this->db->update('users', $postData, "`uid` = {$data['uid']}");
    }
    
    public function delete($uid)
    {
        $result = $this->db->select('SELECT role FROM users WHERE uid = :uid', array(':uid' => $uid));

        if ($result[0]['role'] == 'owner')
        return false;
        
        $this->db->delete('users', "uid = '$uid'");
    }
}