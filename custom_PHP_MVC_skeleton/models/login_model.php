<?php error_reporting(E_ALL); ini_set('display_errors', 1); 

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $sth = $this->db->prepare("SELECT uid, role FROM users WHERE email = :email AND password = :password");
        $sth->execute(array(
            ':email' => $_POST['email'],
            ':password' => Hash::create('sha256', $_POST['password'], HASH_PASSWORD_KEY)
        ));
        
        $data = $sth->fetch();
        
        $count =  $sth->rowCount();
        // log user in
        if ($count > 0) {
            Session::init();
            Session::set('role', $data['role']);
            Session::set('loggedIn', true);
            Session::set('uid_created', $data['uid']);
            header('location: ../dashboard');
        } else {
            header('location: ../login');
        }
        
    }
    
}