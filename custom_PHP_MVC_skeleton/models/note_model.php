<?php error_reporting(E_ALL); ini_set('display_errors', 1); 

class Note_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function noteList()
    {
        return $this->db->select('SELECT * FROM notes WHERE uid_created = :uid_created', 
                array('uid_created' => $_SESSION['uid_created']));
    }
    
    public function noteSingleList($nid)
    {
        return $this->db->select('SELECT * FROM notes WHERE uid_created = :uid_created AND nid = :nid', 
            array('uid_created' => $_SESSION['uid_created'], 'nid' => $nid));
    }
    
    public function create($data)
    {
        $this->db->insert('notes', array(
            'title' => $data['title'],
            'uid_created' => $_SESSION['uid_created'],
            'content' => $data['content'],
            'created' => date('Y-m-d H:i:s') 
        ));
    }
    
    public function editSave($data)
    {
        $postData = array(
            'title' => $data['title'],
            'content' => $data['content'],
            'modified' => date('Y-m-d H:i:s')
        );
        
        $this->db->update('notes', $postData, 
                "`nid` = '{$data['nid']}' AND uid_created = '{$_SESSION['uid_created']}'");
    }
    
    public function delete($id)
    {
        $this->db->delete('notes', "`nid` = {$id} AND uid_created = '{$_SESSION['uid_created']}'");
    }
}