<?php error_reporting(E_ALL); ini_set('display_errors', 1); 

class Dashboard_Model extends Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function xhrStatus() 
    {
        $text = $_REQUEST['status'];
        $uid_created = $_REQUEST['uid_created'];
        $status_date = date('Y-m-d H:i:s');
        $statusData = array(
            'status' => $text,
            'uid_created' => $uid_created,
            'created' => $status_date 
        );
        $userData = array(
            'status' => $text,
            'modified' => $status_date 
        );
        $this->db->insert('statuses', $statusData, "`uid_created` = {$uid_created}");
        $statuses = array('status' => $text, 'stid' => $this->db->lastInsertId(), 'created' => mysqli_real_escape_string($status_date), 'uid_created' => $uid_created);
        $this->db->update('users', mysqli_real_escape_string($userData), "`uid` = {$uid_created}");
        echo json_encode($statuses);
    }
    
    public function xhrGetStatuses()
    {
        $uid_created = $_SESSION['uid_created'];
        $result = $this->db->select('SELECT * FROM statuses WHERE uid_created = :uid_created ORDER BY stid DESC LIMIT 3', 
                array('uid_created' => $_SESSION['uid_created']));
        echo json_encode($result);
    }
    
    public function xhrDeleteStatus()
    {
        $id = (int) $_REQUEST['stid'];
        $date = date('Y-m-d H:i:s');
        $uid_created = $_SESSION['uid_created'];
        $userData = array(
            'status' => "Please Update Your Status. Deleted on: " . $date,
            'modified' => $date
        );
        $this->db->update('users', $userData, "`uid` = {$uid_created}");
        $this->db->delete('statuses', "`stid` = {$id}");
    }

}