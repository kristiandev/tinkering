# README #

Welcome To Kristians Tinkering Playground
Thanks for taking a look at some of my code: 

### I do daily personal development projects to keep my mind fresh and aware of new technologies as well as staying on top of my PHP masters pursuit. (Which a personal goal to program 10,000 hours or more in PHP) ###

I Will be saving a mix of Rust-Lang, Some Ruby, PHP, a few Python projects, Node.js, JQuery, Angular, Reactjs, and Javascript projects in this repository for fun.

### Contribution guidelines ###

* Guidelines: 
If you copy my code, that is okay. I haven't officially licensed any of this code in my public tinkering folder as I have used many methods and resources from other open-source projects from people like mr. Shiffman from The Code Train Youtube Channel, and Jream.com/ (Jesse) from Jream.

### Who do I talk to if I have questions on any of this code? ###

* Kristian Gali <kristiangali@gmail.com>

### What is all of this? ###

* Okay so here is a brief sitemap of what is here ? *

* 1: AI:

 (Artificial Intelligence) -  This is a little experiement I was messing with that has the PHPML (PHP Machine Learning) Library in it.  I was simply creating 1 to 1 relationships with array values and predicting what the next value might be based on the Algorithm that is currently in place.  PHPML is a library that you can download via composer (Do a php composer require search for phpml) and you can use the library to create complex algorithms or prediction-based algorithms. It is a fun project. Try it out! 

* 2: Algo: 

(Algorithm from Ambiverse API) - This is a little project that I created that uses the API from Ambiverse.com. You will need to set up your credentials for OAUTH at the Ambervese website in order for this to work and then use your own Bearer Token. To get a bearer token, go to the Ambiverse API Documentation page and enter your new OAUTH client_id and client_secret in the Post example to get the token and a token will be generated for you. Then use that token within node.js http calls or PHP calls to produce a json response.  The cool thing about ambiverse is it has built-in algorithm libraries based on the English-Language Structure and you can write some awesome scripts with it. 

* 3: Cool IP Stuff:

(Convert IPv6 to IPv4) - Ever wonder why some of your ports on your macbook are open that you are not familiar with ? Do you even know who can track your shared folders? Well I created this cool little script to convert those IPv6 to a regular IPv4 Address and if you don't recognize those stupid IP's that have open ports to your mac, go ahead and add them to your /etc/hosts file under 127.0.0.1. They will need to regenerate a new IPv6 in order to view your stuff and that is usually only your internet providers etc...  You should be aware though that you can block IP's that look at your shared folders by going to your hosts using the following commands: 
cd /etc
sudo nano hosts
go down to the end of the localhost inputs: 
add 
127.0.0.1 theipyouwanttoblock.com (or 123.45.67.89)
press control X and save the hosts file with sudo 
Be very careful when messing with your hosts file, but you should know how to use it to control shared ports.

* 4: Custom PHP MVC Skeleton:

(PHP / AngularJS / PHP-ActiveRecord) - This is a valuable little gem that you can use to start any of your user-login based PHP projects.  I've loved adding Angularjs CDN, but haven't really added any AngularJS functionality yet.  That is in the TODO list.  I've also add PHP-ActiveRecord, but haven't done any ORM PHP query's.  I've just used the PDO wrapper class and modified the models to my liking.  I used the overall setup from Jream.com/lab and added a user post status module using ajax. I've also added the beautiful bootstrap3 css framework with custom css classes.  I also changed out all the login info to use an email instead of username. The tokens table in the database is setup for angularjs directives. I didn't implement them yet, but I will

* Want to test it out or look at the code? 
	I - Get MAMP on your localhost
	II - Look in config file for db info and make sure you create a db called coreframe
	III - Import the database in phpmyadmin or mysequelpro
	IV - basic user and pass with MAMP is root.
	V - After you import the db, go into the controller/index file and uncomment the HASH line and put your admin password in place of the existing password
	VI - login using your email and password.

###

* 5: Ruby On Rails project:  

I built this using Ruby on Rails and generated the User models using the Rails CLI. It was fairly simple. Although I am not a huge advocate of Ruby or Ruby on Rails, lots of people use it so I decided to give it a whirl. PHP is so much better though. Sorry Ruby users, PHP is better in my mind. I could list 1000 reasons why but I don't want to start a complex debate. Yes I am a true PHP advocate through and through and NO it's not going away anytime soon. 

* 6: Rust-Lang PHP MVC compiler: 

I absolutely love the Rust-Lang Cargo CLI as well as the Rust Language. It is freaking awesome and you need to research why I think it will be a big player in 2017. Want to compile this PHP MVC I developed by using codeacademy.com's PHP MVC and rebranding it as my (your) own? simply download rust from rust-lang.org and download this repository or create a pull-request for this.  Navigate to the src file (cd rust-php-mvc/src) and in your terminal type: cargo run
This will generate all the folders and php files in one command. I used the fs (File System) crate from rust's core to build this. 

* 7: Rust Server on port 1111: 

I learned to create a server using the Nickel crate in the Cargo.toml crates library. This was fun. You can do the same thing using node.js, but I wanted to do this using rust to see if I could. It even asks you if you want to spin up the server in the terminal before doing so. Fun, simple project. 

* 8: Starfield: 

This was a tutorial from Eric Shiffman's Youtube Channel using the p5js library to create a simple starfield travel interface. I modified his code and added wormholes as well as a wider canvas and the "Time Travel Effect" image SpeedOfLight.png. If you don't know me, you should know that I believe Time Travel already has existed and Tesla's Radiant energy machine was actually taken by the US government when Tesla died. This project was my dedication to Tesla. He created a machine that I believe had the ability to open a radiant energy field where the machine stood using a reversed-coil and keeping the energy field stable and alive. You might think I'm crazy... Well I don't care. I guess I'm a little weird, but I believe Nikola Tesla had discovered a way to open an energy field that could be used as an actual radiant energy portal. A special thanks to the coding train youtube channel for sparking my interest in this matter. 

* 9: Roman: 

(Using PHP to translate an integer or number into a roman numeral) This is a little bootstrap3 and php script that translates regular integers or floats into roman numerals and vice versa. It string replaces the numerals that output repetative numerals instead of converting the numerals to the correct symbol. This way we can get higher roman numerals such as the year 2017 as shown in the script.


