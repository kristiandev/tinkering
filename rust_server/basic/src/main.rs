#[macro_use] extern crate nickel;
use std::io::{stdin,stdout,Write};
use nickel::{Nickel, HttpRouter};
// Nickel is an out of the box server.
//After running cargo run, this should spin up a server
// and should spin up a server on localhost port: 1111 //
fn main() {
    let mut s=String::new();
    print!("Do you want us to start the server? ");
    let _=stdout().flush();
    stdin().read_line(&mut s).expect("Error reading your question");
    if let Some('\n')=s.chars().next_back() {
        s.pop();
    }
    if let Some('\r')=s.chars().next_back() {
        s.pop();
    }
    println!("Okay: {} It is then! We are starting the server at: 127.0.0.1:1111",s);
    let mut server = Nickel::new();
    server.get("/", middleware!("<h1>Kristian A Gali</h1><br><h2>Kristians Personal Rust Server Works!</h2><br><p>No Need for Mamp or Xampp now. </p> "));
    server.listen("127.0.0.1:1111"); //Similar to nodejs
}
