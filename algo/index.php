<?php error_reporting(E_ALL); ini_set('display_errors', 1);
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.ambiverse.com/v1/entitylinking/analyze",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_SSL_VERIFYHOST => 0,
  CURLOPT_SSL_VERIFYPEER => 0,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"text\" : \"The Latter Day Saint or LDS Church is a great resource to read about God.\"}",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "authorization: Bearer f19a41518f7f4f4da088b7427870ec6834961fee",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  $spit[] = json_decode($response, true);
  foreach($spit as $array) {
    echo "<a href='".$array['matches'][0]['entity']['url']."' target='_blank'>Related Topics for ".$array['matches'][0]['text']."</a>";
    echo "<pre>";
    print_r($array);
    echo "</pre>";
  }
}