<?php require('vendor/autoload.php');

use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;

// sample numbers associated to the targets //
$samples = [[60], [61], [62], [63], [64], [65]];
// trying to predict the target number if there were a sample 66 number
$targets = [3.1, 3.6, 3.8, 4, 4.0, 4.1];

$regression = new SVR(Kernel::LINEAR);
$regression->train($samples, $targets);

echo $regression->predict([66]);

echo "<pre>";

print_r($regression);

echo "</pre>";
?>
