<?php require('vendor/autoload.php');

use Phpml\Metric\ConfusionMatrix;

$actualTargets = ['cat', 'ant', 'cat', 'cat', 'ant', 'bird'];
$predictedTargets = ['ant', 'ant', 'cat', 'cat', 'ant', 'cat'];

$confusionMatrix = ConfusionMatrix::compute($actualTargets, $predictedTargets, ['bird', 'cat']);

echo "<pre>";
print_r($confusionMatrix);

?>