<?php 
    // Function that calculates the roman string to the given number: 
        function dec2roman($f) 
        { 
            // Return false if either $f is not a real number, $f is bigger than 3999 or $f is lower or equal to 0:         
                if(!is_numeric($f) || $f > 3999 || $f <= 0) return false; 

            // Define the roman figures: 
                $roman = array('M' => 1000, 'D' => 500, 'C' => 100, 'L' => 50, 'X' => 10, 'V' => 5, 'I' => 1); 
         
            // Calculate the needed roman figures: 
                foreach($roman as $k => $v) if(($amount[$k] = floor($f / $v)) > 0) $f -= $amount[$k] * $v; 
             
            // Build the string: 
                $return = ''; 
                foreach($amount as $k => $v) 
                { 
                    $return .= $v <= 3 ? str_repeat($k, $v) : $k . $old_k; 
                    $old_k = $k;                 
                } 
             
            // Replace some spacial cases and return the string: 
                return str_replace(array('VIV','LXL','DCD'), array('IX','XC','CM'), $return); 
        } 
     
    // echo dec2romen(1981); 
     
     
    // Function to get the decimal value of a roman string: 
        function roman2dec($str = '') 
        { 
            // Return false if not at least one letter is in the string: 
                if(is_numeric($str)) return false; 
         
            // Define the roman figures: 
                $roman = array('M' => 1000, 'D' => 500, 'C' => 100, 'L' => 50, 'X' => 10, 'V' => 5, 'I' => 1); 
         
            // Convert the string to an array of roman values: 
                for($i = 0; $i < strlen($str); $i++) if(isset($roman[strtoupper($str[$i])])) $values[] = $roman[strtoupper($str[$i])]; 
             
            // Calculate the sum of that array: 
                $sum = 0; 
                while($current = current($values)) 
                { 
                    $next = next($values); 
                    $next > $current ? $sum += $next - $current + 0 * next($values) : $sum += $current; 
                } 
                 
            // Return the value: 
                return $sum; 
        } 
              
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Random List of Romans</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
<hr>
<div class="well well-lg">
<h3>Roman numerals generated from php</h3>
    <ul>
        <li>Point <?php echo dec2roman(1);?>: This is the point one</li>
        <li>Point <?php echo dec2roman(2);?>: This is the point two</li>
        <li>Point <?php echo dec2roman(3);?>: This is the point three</li>
        <li>Point <?php echo dec2roman(4);?>: This is the fourth point</li>
        <li>Point <?php echo dec2roman(5);?>: This is the fifth point</li>
        <li>Year <?php echo dec2roman(2017);?></li>
    </ul>
</div>
</div>
</body>
</html>